package ar.fiuba.tdd.tp0;

import java.util.LinkedList;

public class RPNCalculator {

    public float eval(String expression) {

        if (expression == null) throw new IllegalArgumentException();
        LinkedList<Float> stack = new LinkedList<>();

        for ( String token : expression.split(" ") ) {

            Float tokenNumber = null;
            try {
                tokenNumber = Float.parseFloat(token);
            }
            catch(NumberFormatException e) {}

            if ( tokenNumber != null ) {
                stack.push( tokenNumber );
            } else {
                if ( token.equals( "+" ) ) {
                    if ( stack.size() < 2 ) throw new IllegalArgumentException();
                    Float second = stack.pop();
                    Float first = stack.pop();
                    Float result = first + second;
                    stack.push( result );
                } else if ( token.equals( "-" ) ) {
                    if ( stack.size() < 2 ) throw new IllegalArgumentException();
                    Float second = stack.pop();
                    Float first = stack.pop();
                    Float result = first - second;
                    stack.push( result );
                } else if ( token.equals( "*" ) ) {
                    if ( stack.size() < 2 ) throw new IllegalArgumentException();
                    Float second = stack.pop();
                    Float first = stack.pop();
                    Float result = first * second;
                    stack.push( result );
                } else if ( token.equals( "/" ) ) {
                    if ( stack.size() < 2 ) throw new IllegalArgumentException();
                    Float second = stack.pop();
                    Float first = stack.pop();
                    Float result = first / second;
                    stack.push( result );
                } else if ( token.equals( "MOD" ) ) {
                    if ( stack.size() < 2 ) throw new IllegalArgumentException();
                    Float second = stack.pop();
                    Float first = stack.pop();
                    Float result = first % second;
                    stack.push( result );
                } else if ( token.equals( "**" ) ) {
                    if ( stack.size() == 0 ) throw new IllegalArgumentException();
                    if ( stack.size() > 1 ) {
                        Float result = stack.pop();
                        while ( stack.size() > 0 ) {
                            result *= stack.pop();
                        }
                        stack.push( result );
                    }
                } else if ( token.equals( "++" ) ) {
                    if ( stack.size() == 0 ) throw new IllegalArgumentException();
                    if ( stack.size() > 1 ) {
                        Float result = stack.pop();
                        while ( stack.size() > 0 ) {
                            result += stack.pop();
                        }
                        stack.push( result );
                    }
                } else if ( token.equals( "//" ) ) {
                    if ( stack.size() == 0 ) throw new IllegalArgumentException();
                    if ( stack.size() > 1 ) {
                        Float result = stack.pop();
                        while ( stack.size() > 0 ) {
                            result /= stack.pop();
                        }
                        stack.push( result );
                    }
                } else if ( token.equals( "--" ) ) {
                    if ( stack.size() == 0 ) throw new IllegalArgumentException();
                    if ( stack.size() > 1 ) {
                        Float result = stack.pop();
                        while ( stack.size() > 0 ) {
                            result -= stack.pop();
                        }
                        stack.push( result );
                    }
                } else {
                    throw new IllegalArgumentException();
                }

            }

        }
        return stack.pop();
    }
}
